using UnityEngine;
#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif

namespace Technetium
{
    public class TimerSpriteAnimator : ASpriteAnimator, IProvideFrameRate
    {
        [Min(0f)]
        [SerializeField] private float fps = 30f;

        public float Fps => fps;

        private float timer;

        
        public void SetFps(float targetFps) 
            => fps = targetFps;

        #if ODIN_INSPECTOR
        [Button]
        #endif
        public override void StartClip(ReusableClip reusableClip, int startFrame = 0)
        {
            base.StartClip(reusableClip, startFrame);
            if (reusableClip.GetFps(out var fpsPreference)) fps = fpsPreference;
            timer = 0;
        }

        private void Update()
        {
            if (!clip || !spriteMap) return;
            if (fps <= 0) return;
            timer += Time.deltaTime;
            var timePerFrame = 1 / fps;
            while (timer > timePerFrame)
            {
                IncrementFrame();
                timer -= timePerFrame;
            }

            if (clip.UpdateIndex(ref Index, Frame)) PushFrame();
        }
    }
}
