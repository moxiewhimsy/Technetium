using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Technetium
{
    [System.Obsolete("Replace with TimerSpriteAnimator or ASpriteAnimator")]
    public class SpriteAnimator : TimerSpriteAnimator
    {
    }
}
