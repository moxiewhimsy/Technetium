using UnityEngine;
using Object = UnityEngine.Object;

namespace Technetium
{
    public class SyncAnimator : ASpriteAnimator
    {
        [SerializeField] private Object objectProvider;
        [Tooltip("Also sync the clip if provider is an animator")]
        [SerializeField] private bool syncClip = true;

        #if UNITY_EDITOR
        [Space, Header("Editor")]
        [SerializeField] private TimerSpriteAnimator setFromAnimator;
        #endif

        private void OnDisable()
        {
            if (!objectProvider || objectProvider is not IProvideFrameRate source) return;
            source.FrameChanged -= OnFrameChanged;
            if (syncClip && objectProvider is ASpriteAnimator animator) animator.ClipChanged -= SwapClip;
        }

        private void OnEnable()
        {
            if (!objectProvider || objectProvider is not IProvideFrameRate source) return;
            source.FrameChanged += OnFrameChanged;
            if (syncClip && objectProvider is ASpriteAnimator animator) animator.ClipChanged += SwapClip;
        }
        
        private void OnFrameChanged(int incomingFrame)
        {
            Frame = incomingFrame;
            if (clip && clip.UpdateIndex(ref Index, Frame)) PushFrame();
        }

        #if UNITY_EDITOR
        private void OnValidate()
        {
            if (!setFromAnimator) return;
            objectProvider = setFromAnimator;
            setFromAnimator = null;
        }
        #endif
    }
}
