using UnityEngine;
using UnityEngine.UI;

namespace Technetium
{
    [RequireComponent(typeof(Image))]
    public class PushSpriteToUI : MonoBehaviour, IPushSprite
    {
        [SerializeField] private Image image;
        [SerializeField] private ASpriteAnimator animator;
        [SerializeField] private bool setToNativePixelSize;
        [SerializeField] private bool setEmptyToClear;
        [SerializeField] private Color fillColor = Color.white;

        private void Reset()
        {
            TryGetComponent(out image);
            TryGetComponent(out animator);
        }

        private void Awake()
        {
            if (!image) TryGetComponent(out image);
        }
        
        private void OnDisable()
        {
            if (animator) animator.ChangeSprite -= SetSprite;
        }

        private void OnEnable()
        {
            if (animator) animator.ChangeSprite += SetSprite;
        }

        public void SetSprite(Sprite sprite)
        {
            if (!image) return;
            image.sprite = sprite;
            image.color = sprite || !setEmptyToClear ? fillColor : Color.clear;
            if (sprite && setToNativePixelSize) image.SetNativeSize();
        }
    }
}
