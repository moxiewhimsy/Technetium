using UnityEngine;

namespace Technetium
{
	public interface IPushSprite
	{
		void SetSprite(Sprite sprite);
	}
}