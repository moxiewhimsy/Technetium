using System.Collections.Generic;
using System.Linq;
#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif
using UnityEngine;
using UnityEngine.Serialization;

namespace Technetium
{
    
    [CreateAssetMenu(menuName = "Sprite Animator/Sprite Map", fileName = "New Sprite Map", order = 0)]
    public class SpriteMap : ScriptableObject
    {
        [FormerlySerializedAs("sprites")] [SerializeField] private List<NamedFrame> frames;
        
        #if UNITY_EDITOR
        #if ODIN_INSPECTOR
        [InlineEditor]
        #endif
        [SerializeField] private List<ReusableClip> clips = new();

        [FormerlySerializedAs("addFrameSprites")]
        [Header("Editor Actions")]
        #if ODIN_INSPECTOR
        [PreviewField]
        #endif
        [SerializeField]
        private List<Sprite> addSprites = new();
        [SerializeField] private bool autoAddOnValidate = true;
        
        #if ODIN_INSPECTOR
        [Button, HideIf(nameof(autoAddOnValidate))]
        #endif
        [ContextMenu(nameof(AddSprites))]
        private void AddSprites()
        {
            if (addSprites.Count <= 0) return;
            var sprites = addSprites.ToArray();
            addSprites.Clear();
            TryAdd(sprites);
        }

        private readonly Dictionary<Sprite, NamedFrame> cache = new();
        #endif

        public bool Contains(Sprite sprite) => frames.Any(s => s.image == sprite);

        public bool Get(KeyId id, out Sprite sprite)
        {
            var frame = frames.FirstOrDefault(s => s.name == id.name);
            sprite = frame.image;
            return !frame.IsEmpty;
        }
        
        public bool GetFrame(int index, ReusableClip clip, out Sprite sprite)
        {
            sprite = clip && clip.Get(index, out var id) && Get(id, out sprite) ? sprite : null;
            return sprite;
        }
        
        public KeyId GetId(Sprite sprite)
        {
            #if UNITY_EDITOR
            if (cache.TryGetValue(sprite, out var value))
                return NamedFrame.GetId(value);
            #endif
            var frame = frames.Find(s => s.image == sprite);
            return NamedFrame.GetId(frame);
        }

        public IEnumerable<KeyId> GetIds(IEnumerable<Sprite> sprites)
        {
            foreach (var sprite in sprites)
            {
                if (TryGetId(sprite, out var id)) yield return id;
            }
        }

        public bool TryGetId(Sprite check, out KeyId id)
        {
            var search = frames.Where(s => s.image == check).ToArray();
            if (search.Length <= 0)
            {
                id = KeyId.Empty;
                return false;
            }

            id = search.Select(NamedFrame.GetId).First();
            return true;
        }
        
        #if UNITY_EDITOR
        public bool TryAdd(params Sprite[] incoming)
        {
            var existing = frames.Select(s => s.image).ToList();
            incoming = incoming.SkipWhile(existing.Contains).ToArray();
            if (incoming.Length <= 0) return false;
            frames.AddRange(incoming.Select(NamedFrame.FromSprite));
            AssignIndices();
            RefreshCache();
            return true;
        }
        
        #if ODIN_INSPECTOR
        [Button]
        #endif
        [ContextMenu(nameof(SpriteClipCoverage))]
        private void SpriteClipCoverage()
        {
            var spriteIds = frames.Select(NamedFrame.GetId).ToArray();
            var clipIds = clips.SelectMany(clip => clip.GetUniqueKeys()).Distinct().ToArray();
            var danglingIds = spriteIds.Where(id => !clipIds.Contains(id)).ToArray();
            var missingIds = clipIds.Where(id => !spriteIds.Contains(id)).ToArray();
            var danglingFrames = string.Join(", ", danglingIds.Select(id => id.name));
            var missingFrames = string.Join(", ", missingIds.Select(id => id.name));
            if (danglingIds.Length <= 0 && missingIds.Length <= 0)
            {
                var clipNames = string.Join(", ", clips.Select(clip => clip.name));
                Debug.Log($"No dangling or missing frames in {name} amongst {clipNames}.", this);
                return;
            }
            if (danglingIds.Any())
                Debug.LogWarning($"{name} map has the following Ids not referenced in any clip: {danglingFrames}", this);
            else Debug.Log($"No dangling frames in {name}");
            if (missingIds.Any()) 
                Debug.LogWarning($"{name} map is missing the following Ids from clips: {missingFrames}.", this);
            else Debug.Log($"No missing Ids found in clips for {name} map.");
        }
        
        private void OnValidate()
        {
            RefreshCache();
            if (autoAddOnValidate) AddSprites();
        }

        [ContextMenu(nameof(AssignIndices))]
        private void AssignIndices()
        {
            for (int i = 0; i < frames.Count; i++)
            {
                var frame = frames[i];
                frame.index = i + 1;
                frames[i] = frame;
            }
        }

        private void RefreshCache()
        {
            cache.Clear();
            foreach (var namedFrame in frames)
            {
                if (cache.ContainsKey(namedFrame.image)) continue;
                cache.Add(namedFrame.image, namedFrame);
            }
        }
        #endif // UNITY_EDITOR
    }
}
