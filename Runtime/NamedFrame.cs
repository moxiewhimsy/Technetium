#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif
using UnityEngine;

namespace Technetium
{
    [System.Serializable]
    public struct NamedFrame
    {
        #if ODIN_INSPECTOR
        [VerticalGroup("row1/left")]
        #endif
        public string name;
        #if ODIN_INSPECTOR
        [VerticalGroup("row1/left")]
        #endif
        public int index;
        #if ODIN_INSPECTOR
        [HideLabel]
        [PreviewField(50, ObjectFieldAlignment.Right)]
        [HorizontalGroup("row1", 64), VerticalGroup("row1/right")]
        #endif
        public Sprite image;

        public bool IsEmpty => !image && string.IsNullOrEmpty(name);

        public static NamedFrame FromSprite(Sprite sprite) => new() { image = sprite, name = sprite.name };
        public static KeyId GetId(NamedFrame frame) => new() { name = frame.name };
    }
}
