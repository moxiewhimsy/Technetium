using UnityEngine;
#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif

namespace Technetium
{
    public abstract class ASpriteAnimator : MonoBehaviour
    {
        [SerializeField] protected SpriteMap spriteMap;
        [SerializeField] protected ReusableClip clip;
        [SerializeField] protected bool pushEmptySprites;
        
        protected int Index;
        
        #if ODIN_INSPECTOR 
        [ShowInInspector, ReadOnly]
        #endif
        protected int Frame;

        public event System.Action<Sprite> ChangeSprite;
        public event System.Action<int> FrameChanged;
        public event System.Action<ReusableClip> ClipChanged; 

        public void SetFrame(int value)
        {
            if (Equals(Frame, value)) return;
            Frame = value;
            FrameChanged?.Invoke(Frame);
        }
        
        public void SetMap(SpriteMap map)
        {
            if (spriteMap == map) return;
            spriteMap = map;
            PushFrame();
        }
        
        #if ODIN_INSPECTOR 
        [ShowInInspector, ReadOnly]
        #endif
        public virtual void StartClip(ReusableClip reusableClip, int startFrame = 0)
        {
            if (startFrame >= 0)
            {
                SetFrame(startFrame);
            }
            SwapClip(reusableClip);
        }

        #if ODIN_INSPECTOR
        [Button]
        #endif
        public void SwapClip(ReusableClip reusableClip)
        {
            if (!Equals(reusableClip, clip))
            {
                clip = reusableClip;
                ClipChanged?.Invoke(clip);
            }
            clip.UpdateIndex(ref Index, Frame);
            PushFrame();
        }
        
        protected void PushFrame()
        {
            if (!GetFrame(out var sprite) && !pushEmptySprites) return;
            ChangeSprite?.Invoke(sprite);
        }

        private bool GetFrame(out Sprite sprite)
        {
            if (spriteMap) return spriteMap.GetFrame(Index, clip, out sprite);
            sprite = null;
            return false;
        }

        protected void IncrementFrame() => SetFrame(Frame + 1);
    }
}