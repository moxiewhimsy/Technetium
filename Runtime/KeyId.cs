namespace Technetium
{
	[System.Serializable]
	public struct KeyId
	{
		public string name;

		public static KeyId Empty => new KeyId();
	}
}