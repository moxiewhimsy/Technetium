using System.Collections.Generic;
using System.Linq;
#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif
using UnityEngine;

namespace Technetium
{
    [CreateAssetMenu(menuName = "Sprite Animator/Clip", fileName = "New Reusable Clip", order = 0)]
    public class ReusableClip : ScriptableObject
    {
        [SerializeField] private List<KeyId> keys = new List<KeyId>();

        [SerializeField] private bool isLoop;

        [Min(1)]
        [SerializeField] private int framesPerKey = 1;
        [SerializeField] private bool hasFps;
        #if ODIN_INSPECTOR
        [ShowIf(nameof(hasFps))]
        #endif
        [SerializeField] private float fps = -1f;
        
        #if ODIN_INSPECTOR
        [ReadOnly, ShowInInspector]
        #endif
        private int FrameCount => framesPerKey * keys.Count;
        
        public int Count => keys.Count;

        public IEnumerable<KeyId> GetUniqueKeys() => keys.Distinct();

        #if UNITY_EDITOR
        [Header("Editor Actions")]
        [SerializeField] private List<Sprite> addSprites = new();
        [SerializeField] private SpriteMap withMap;
        #endif


        public void Add(params KeyId[] ids) => keys.AddRange(ids);

        public bool Get(int index, out KeyId id)
        {
            if (keys.Count <= 0 || index < 0 || index >= keys.Count)
            {
                id = KeyId.Empty;
                return false;
            }

            id = keys[index];
            return true;
        }

        public bool GetFps(out float fpsPreference)
        {
            fpsPreference = fps;
            return hasFps;
        }

        public bool UpdateIndex(ref int index, int frame, int offset = 0)
        {
            var oldIndex = index;
            index = index < 0 ? 0 : index;
            if (keys.Count == 0 || FrameCount <= 0)
                return false;

            frame = frame < FrameCount ? frame : isLoop ? frame % FrameCount : FrameCount - 1;
            index = frame / framesPerKey;
            index = index < keys.Count ? index : isLoop ? index % keys.Count : keys.Count - 1;
            return index != oldIndex;
        }

        #if UNITY_EDITOR
        private void OnValidate()
        {
            if (!withMap || !addSprites.Any()) return;
            var sprites = addSprites.ToArray();
            addSprites.Clear();
            keys.AddRange(withMap.GetIds(sprites));
        }
        #endif
    }
}