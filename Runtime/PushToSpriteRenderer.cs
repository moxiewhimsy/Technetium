using UnityEngine;

namespace Technetium
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class PushToSpriteRenderer : MonoBehaviour, IPushSprite
    {
        [SerializeField] private SpriteRenderer spriteRenderer;
        [SerializeField] private ASpriteAnimator animator;
        [SerializeField] private bool setEmptyToClear;
        [SerializeField] private Color fillColor = Color.white;

        private void Reset()
        {
            TryGetComponent(out spriteRenderer);
            TryGetComponent(out animator);
        }

        private void Awake()
        {
            if (!spriteRenderer) TryGetComponent(out spriteRenderer);
        }

        private void OnDisable()
        {
            if (animator) animator.ChangeSprite -= SetSprite;
        }

        private void OnEnable()
        {
            if (animator) animator.ChangeSprite += SetSprite;
        }

        public void SetSprite(Sprite sprite)
        {
            if (!spriteRenderer) return;
            spriteRenderer.sprite = sprite;
            spriteRenderer.color = sprite || !setEmptyToClear ? fillColor : Color.clear;
        }
    }
}
