namespace Technetium
{
    public interface IProvideFrameRate
    {
        event System.Action<int> FrameChanged;
    }
}