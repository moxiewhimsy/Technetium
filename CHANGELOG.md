# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
+ Sprite Animators can optionally push empty sprites when getting an empty frame. Empty frames occur when a clip is complete or mismatches the sprite map. 
+ Optionally set ui image to native size of sprite set to it  
+ Sync Animator component follows the same frame rate as an animator with IProvideFrameRate interface
### Changed
+ Extract and refactor bulk of Sprite Animator functionality from legacy Sprite Animator into abstract Sprite Animator
+ Rename and refactor legacy Sprite Animator to Timer Sprite Animator which provides frame rate
## [0.0.22] - 2022-08-24
### Added
+ Sprite Clip Coverage button (or context item) on sprite maps checks for:
  + Dangling frames which are in the map and not in any clip
  + Missing frames which are in a clip and not in the map
+ Reusable clips can have a preferred frames per second, which the Sprite Animator can use
+ Sprite Animator pauses when set to zero frames per second 
+ StartClip sets a SpriteAnimator's clip, optionally resetting the timer, or specifying a start frame
+ SwapClip sets a SpriteAnimator's clip without changing frame timing
+ Both PushToSpriteRenderer and PushSpriteToUI optionally clear the color when sprite is empty
### Changed
+ Reusable clips do not always loop
### Removed
+ Validate Clips button (and context item) on sprite maps. Use Sprite Clip Coverage instead.
## [0.0.12] - 2022-08-11
### Added
+ SetMap mutator for SpriteAnimator
## [0.0.11] - 2022-08-08
### Changed
+ (EDITOR) Improved Create Asset Menu entry and filename
### Fixed
+ (EDITOR) Inline Editor only available with Odin Inspector
## [0.0.10] - 2022-08-08
### Added
+ (EDITOR) SpriteMap can hold clips and validate whether it has a named frame for every id in those clips.
### Changed
+ Renamed SpriteMap from SpriteKeyMap. Which is how I thought of it anyhow.
+ NamedFrame is only concerned with whether it has a sprite, and whether it has a name. Simplified.
+ Ported 
  + GetFrame method into SpriteMap from SpriteClip
  + FrameCount property and framesPerKey field into ReusableClip from SpriteClip 
  + UpdateIndex method into ReusableClip from SpriteClip
+ SpriteAnimator behaviour is now given a map and a clip instead of an explicit SpriteClip
+ (EDITOR) Sprite adding experience improved on SpriteMap
+ (EDITOR) NamedFrame has an improved layout when using Odin Inspector
+ (EDITOR) SpriteAnimator no longer has a (misleading) "Derived" header, and does not expose index in Odin Inspector  
### Removed
+ SpriteClip is fully unnecessary now. Would have marked Obsolete and given transition instructions if this weren't in alpha.

## [0.0.6] - 2022-08-06
### Changed
+ SpriteMap uses the name string of an id to fetch frames
+ KeyId and NamedFrame no longer care about index, only names
+ (EDITOR) SpriteMap has a cleaner context menu item to Assign Indices

## [0.0.4] - 2022-08-03
### Added
+ PushToSpriteRenderer behaviour subscribes to SpriteAnimator and updates the sprite of a SpriteRenderer
### Changed
+ Renamed PushSpriteToUI from RenderSpriteToUI
+ Reworked PushSpriteToUI to work more like PushToSpriteRenderer

## [0.0.2] - 2022-08-02
### Added
+ structs 
  + KeyId should identify a specific frame in every SpriteMap
  + NamedFrame holds a sprite and a name for that sprite
+ scriptable objects
  + ReusableClip holds the ids for key frames of a clip
  + SpriteKeyMap holds named sprites and editor UI to easily add more
  + SpriteClip holds a ReusableClip, a SpriteMap, and how many frames each key gets
+ behaviours
  + SpriteAnimator sends frames to any subscribers given a SpriteClip and an fps
  + RenderSpriteToUI subscribes to SpriteAnimator and updates a UI Image component
+ interfaces
  + IRenderSprite describes subscribers to SpriteAnimator which each must set a sprite at some point